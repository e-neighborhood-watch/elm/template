module Main exposing
  ( main
  )


import Browser
import Html


type alias Model =
  {}


type Message =
  Noop


init : a -> ( Model, Cmd Message )
init _ =
  ( {}
  , Cmd.none
  )


update : Message -> Model -> ( Model, Cmd Message )
update message prevModel =
  case
    message
  of
    Noop ->
      ( prevModel
      , Cmd.none
      )


view : Model -> Browser.Document Message
view {} =
  { title =
    "Hello world"
  , body =
    []
  }


subscriptions : Model -> Sub Message
subscriptions {} =
  Sub.none


main : Program () Model Message
main =
  Browser.document
    { init = init
    , update = update
    , view = view
    , subscriptions = subscriptions
    }
